<div class="row p-5">
    <div class="col-sm-12 col-lg-3"></div>
    <div class="col-sm-12 col-lg-6 ">
        <div class="container shadow-lg bg-transparent">
            <div class="card bg-transparent border-0 pt-4">
                <?php 
                    if ($user['User']['profile_picture']) {
                        $profile = $user['User']['profile_picture'];
                    } else {
                        $profile = 'default-profile.png';
                    }
                ?>
              <?= $this->Html->image(
                            'profiles/'.$profile, 
                            ['alt' => '',
                                'class' => 'rounded-circle align-self-center border border-white',
                                'width' => '125',
                                'height' => '125'
                            ]
                        ) ?>
              <div class="card-body text-center pt-2">
                <span class="font-italic text-muted">@<?= $user['User']['username'] ?></span>
                <h5 class="card-title font-weight-bold route-user"
                    data-userId="<?= $user['User']['id']?> ">
                    <?= $user['User']['first_name'] ?>
                    <?= $user['User']['last_name'] ?>
                </h5>
              </div>
            </div>
        </div>
        <div class="container shadow-lg bg-transparent mt-2 py-2">
            <h5>Followers
                <span class="badge badge-light ml-2">
                    <?= $this->Paginator->counter('{:count}') ?>
                </span>
            </h5>
            <hr>
            <div class="row">
                <div class="col-sm-12" >
                    <?php if ($followers): ?>
                    <?php foreach ($followers as $key => $follower): ?>
                    <div class="card">
                        <div class="card-body py-2">
                            <?php 
                                if ($follower['User']['profile_picture']) {
                                    $profile = $follower['User']['profile_picture'];
                                } else {
                                    $profile = 'default-profile.png';
                                }
                            ?>
                            <?= $this->Html->image(
                                'profiles/'.$profile, 
                                ['alt' => '',
                                    'class' => 'rounded-circle d-inline-block',
                                    'width' => '50',
                                    'height' => '50'
                                ]
                            ) ?>
                            <div class="d-inline-block align-bottom pl-2">
                                <h6 class="font-weight-bold m-0 route-user"
                                    data-userId="<?= $follower['User']['id']?>" >
                                    <?= $follower['User']['first_name'] ?> 
                                    <?= $follower['User']['last_name'] ?>
                                </h6>
                                <h6 class="small font-italic text-muted">
                                    @<?= $follower['User']['username'] ?>
                                </h6>
                            </div>
                            <div class="d-inline-block align-middle float-right line-hieght-50">
                                <?php 
                                    if ($follower['FollowsJoin']['id']) {
                                        $followLabel = 'Unfollow';
                                        $class = 'btn-secondary px-2';
                                    } else {
                                        $followLabel = 'Follow';
                                        $class = 'btn-outline-secondary px-3';
                                    }
                                ?>
                                <?= $this->Form->postLink(
                                    $followLabel,
                                    [
                                        'controller' => 'follows',
                                        'action' => 'follow',
                                        $follower['User']['id']
                                    ],
                                    ['class' => "btn btn-sm {$class}"]
                                ) ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                    <?= $this->element('common/pagination') ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-lg-3"></div>
</div>
<?= $this->Html->script('follows') ?>