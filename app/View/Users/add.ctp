<div class="users form mt-3">
    <div class="row h-100">
        <div class="col-sm-12 my-auto">
            <div class="card card-block w-50 mx-auto">
                <div class="card">
                    <div class="card-header  align-self-center bg-transparent">
                        <?= $this->Html->image(
                            'logo.png', 
                            [
                                'alt' => '',
                                'class' => 'rounded-circle',
                                'width' => '100',
                                'height' => '100'
                            ]
                        ) ?>
                    <h4>MicroBlog</h4>
                    </div>
                    <div class="card-body">
                        <?= $this->Form->create(
                                'User', 
                                [
                                    'autocomplete' => 'off',
                                    'novalidate' => true
                                ]
                            ) ?>
                        <div class="form-row">
                            <div class="form-group col-md-6 col-sm-12">
                                <?= $this->Form->input(
                                    'first_name',
                                    [
                                        'label' => 'First Name',
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter first name'
                                    ]
                                ) ?>
                            </div>
                            <div class="form-group col-md-6 col-sm-12 ">
                                <?= $this->Form->input(
                                    'last_name',
                                    [
                                        'label' => 'Last Name',
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter last name'
                                    ]
                                ) ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6 col-sm-12">
                                <?= $this->Form->input(
                                    'username',
                                    [
                                        'label' => 'Username',
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter username'
                                    ]
                                ) ?>
                            </div>
                            <div class="form-group col-md-6 col-sm-12 ">
                                <?= $this->Form->input(
                                    'email',
                                    [
                                        'label' => 'Email',
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter email'
                                    ]
                                ) ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6 col-sm-12">
                                <?= $this->Form->input(
                                    'password',
                                    [
                                        'label' => 'Password',
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter password'
                                    ]
                                ) ?>
                            </div>
                            <div class="form-group col-md-6 col-sm-12 ">
                                <?= $this->Form->input(
                                    'password_confirm',
                                    [
                                        'label' => 'Confirm Password',
                                        'class' => 'form-control',
                                        'placeholder' => 'Re-enter password',
                                        'type' => 'password'
                                    ]
                                ) ?>
                            </div>
                        </div>
                        <?php 
                            $options = [
                                'label' => 'Submit',
                                'class' => 'btn btn-primary btn-block',
                                'div' => [
                                    'class' => 'text-center px-5 mt-3',
                                ]
                            ];
                            echo $this->Form->end($options);
                        ?>
                        <div class="text-center font-italic pt-3 small">
                            <?= $this->Html->link(
                                'Back to Log in',
                                ['controller' => 'users', 'action' => 'login'],
                                ['class' => 'font-italic']
                            ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>