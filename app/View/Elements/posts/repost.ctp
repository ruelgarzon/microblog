<div class="modal" id="repostModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create('Post', 
                [
                    'id' => 'RepostForm',
                    'novalidate' => true,
                    'url' => ['controller' => 'posts', 'action' => 'repost']
                ]
            ) ?>
                <?= $this->Form->input('parent_id', ['type' => 'hidden']) ?>
                <div class="modal-header">
                    <h4 class="modal-title">Repost</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        &times;
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <?=  $this->Form->input(
                            'content', 
                            [
                                'label' => 'Repost Caption',
                                'rows' => '4',
                                'class' => 'form-control'
                            ]
                        ) ?>
                    </div>
                </div>
            <?= $this->Form->end([
                    'label' => 'Save',
                    'class' => 'btn btn-outline-primary',
                    'div' => [ 'class' => 'modal-footer' ]
                ]
            ) ?>
            
        </div>
    </div>
</div>