<?php if ($followings): ?>
    <?php foreach ($followings as $key => $following): ?>
    <div class="card">
        <div class="card-body py-2">
            <?php 
                if ($following['User']['profile_picture']) {
                    $profile = $following['User']['profile_picture'];
                } else {
                    $profile = 'default-profile.png';
                }
            ?>
            <?= $this->Html->image(
                'profiles/'.$profile, 
                ['alt' => '',
                    'class' => 'rounded-circle d-inline-block',
                    'width' => '50',
                    'height' => '50'
                ]
            ) ?>
            <div class="d-inline-block align-bottom pl-2">
                <h6 class="font-weight-bold m-0 route-user"
                    data-userId="<?= $following['User']['id']?> ">
                    <?= $following['User']['first_name'] ?> 
                    <?= $following['User']['last_name'] ?>
                </h6>
                <h6 class="small font-italic text-muted">
                    @<?= $following['User']['username'] ?>
                </h6>
            </div>
            <?php 
                if ($following['User']['id'] == $authUser['id']) {
                    $displayClass = 'd-none';
                } else {
                    $displayClass = 'd-inline-block';
                }
            ?>
            <div class="<?= $displayClass ?> align-middle float-right line-hieght-50">
                <?php 
                    if ($following['FollowsJoin']['id']) {
                        $followLabel = 'Unfollow';
                        $class = 'btn-secondary px-2';
                    } else {
                        $followLabel = 'Follow';
                        $class = 'btn-outline-secondary px-3';
                    }
                ?>
                <?= $this->Form->postLink(
                    $followLabel,
                    [
                        'controller' => 'follows',
                        'action' => 'follow',
                        $following['User']['id']
                    ],
                    ['class' => "btn btn-sm {$class}"]
                ) ?>
            </div>
        </div>
    </div>
    <?php endforeach ?>
<?php else: ?>
        <div class="card">
            <div class="card-body text-center  mt-4">
                <?php if($editAccess): ?>
                    <p class="card-text">You seem to be not following someone. 
                    Start now by searching in the search bar or hit the button below.</p>
                    <?= $this->Html->link(
                        'Start Searching Now',
                        ['controller' => 'searches', 'action' => 'index'],
                        ['class' => 'btn btn-outline-info btn-block mt-3']
                    ) ?>
                <?php else: ?>
                     <p class="card-text">
                         User seem to be not following someone. 
                     </p>
                <?php endif ?>
            </div>
        </div>
<?php endif ?>
<?php if ($followingCount > 10): ?>
    <small class="form-text text-muted text-right">
        Showing only <b>10</b> of <b><?= $followingCount ?></b>
    </small>
    <?= $this->Html->link(
        'View All Following',
        ['controller' => 'followings', 'action' => 'index'],
        ['class' => 'btn btn-outline-info btn-block mt-3']
    ) ?>
<?php endif ?>