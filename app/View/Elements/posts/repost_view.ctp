<div class="container mx-5 px-5">
    <div class="card mr-5">
        <?php if ($post['Reposted']['deleted'] == 0): ?>
        <div class="card-body py-2">
            <?php 
                if ($post['RepostedUser']['profile_picture']) {
                    $repostdProfile = $post['RepostedUser']['profile_picture'];
                } else {
                    $repostdProfile = 'default-profile.png';
                }
            ?>
            <?=  $this->Html->image(
                    'profiles/'.$repostdProfile, 
                    ['alt' => '',
                        'class' => 'rounded-circle d-inline-block',
                        'width' => '50',
                        'height' => '50'
                    ]
                ) ?>
            <div class="d-inline-block align-bottom pl-2">
                <h6 class="font-weight-bold m-0 route-user"
                    data-userId="<?= $post['RepostedUser']['id'] ?>">
                    <?= $post['RepostedUser']['first_name']?>
                    <?= $post['RepostedUser']['last_name']?>
                    <span class="small font-italic text-muted" >@<?= $post['RepostedUser']['username']?></span>
                </h6>
                <h6 class="small font-italic text-muted">
                    <time class="timeago" datetime="<?= $post['Reposted']['created']?>"></time>
                </h6>
            </div>
        </div>
        <div class="card mb-4 mr-4 ml-5 pl-4 border-0">
            <div class="card-body p-0">
                <p class="card-text"><?= h($post['Reposted']['content']) ?></p>
            </div>
            <?php 
            if ($post['Reposted']['image_name']) {
                echo  $this->Html->image(
                    'posts/'. $post['Reposted']['image_name'], 
                    ['alt' => '',
                        'class' => 'img-fluid img-thumbnail',
                            'style' => ['max-width : 300px']

                    ]
                );
            } ?>
        </div>
        <?php else: ?>
            <div class="card border-0">
                <div class="card-body py-3 p-0">
                    <p class="card-text font-italic font-weight-light text-center">
                            <span class="oi oi-warning"></span>
                        This content has been deleted by the author.
                    </p>
                </div>
            </div>
        <?php endif ?>
    </div>
</div>