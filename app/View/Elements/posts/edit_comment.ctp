<div class="modal" id="editComment">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="editCommentForm" novalidate="novalidate">
                <input id="CommentId" type="hidden" name="data[PostComment][id]">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Comment</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        &times;
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="comment"></label>
                        <input type="text" name="data[PostComment][comment]" 
                        class="form-control" id="CommentContent" 
                        required maxlength="140">
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="submitCommentForm" type="submit" class="btn btn-outline-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>