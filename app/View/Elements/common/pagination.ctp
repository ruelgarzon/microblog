<nav aria-label="..." class="mt-3">
    <ul class="pagination justify-content-end">
        <?= $this->Paginator->prev(
                $this->Html->tag('span', '', 
                    ['class' => 'oi oi-caret-left']
                ),
                [
                    'tag' => 'li', 
                    'class' => 'page-item',
                    'escape' => false
                ], 
                null, 
                [
                    'tag' => 'li',
                    'class' => 'disabled page-item',
                    'disabledTag' => 'a'
                ]
            ) ?>
        <?= $this->Paginator->numbers(
                [
                    'separator' => '',
                    'currentTag' => 'a', 
                    'currentClass' => 'active',
                    'tag' => 'li', 
                    'class' => 'page-item',
                    'first' => 1
                ]
            ) ?>
        <?= $this->Paginator->next(
                $this->Html->tag('span', '', 
                    ['class' => 'oi oi-caret-right']
                ), 
                [
                    'tag' => 'li',
                    'currentClass' => 'disabled',
                    'class' => 'page-item',
                    'escape' => false
                ], 
                null, 
                [
                    'tag' => 'li',
                    'class' => 'disabled page-item', 
                    'disabledTag' => 'a'
                ]
            ) ?>
    </ul>
</nav>