<div class="modal" id="changePasswordModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->Form->create('User', 
                [
                    'enctype'=> 'multipart/form-data',
                    'id' => 'ProfileEditForm',
                    'novalidate' => true,
                    'url' => ['controller' => 'users', 'action' => 'changePassword']
                ]
            ) ?>
            <?= $this->Form->hidden('id', ['default'=> $authUser['id']]) ?>
            <div class="modal-header">
                <h4 class="modal-title">Change Password</h4>
                <button type="button" class="close" data-dismiss="modal">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <div class="form-row">
                    <div class="form-group col-md-6 col-sm-12">
                        <?= $this->Form->input(
                            'password',
                            [
                                'label' => 'Password',
                                'class' => 'form-control',
                                'placeholder' => 'Enter password'
                            ]
                        ) ?>
                    </div>
                    <div class="form-group col-md-6 col-sm-12 ">
                        <?= $this->Form->input(
                            'password_confirm',
                            [
                                'label' => 'Confirm Password',
                                'class' => 'form-control',
                                'placeholder' => 'Re-enter password',
                                'type' => 'password'
                            ]
                        ) ?>
                    </div>
                </div>
            </div>
            <?= $this->Form->end([
                    'label' => 'Save',
                    'class' => 'btn btn-outline-primary',
                    'div' => [ 'class' => 'modal-footer' ]
                ]
            )?>
        </div>
    </div>
</div>