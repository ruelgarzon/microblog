<?php
App::uses('User', 'Model');
App::uses('Post', 'Model');
App::uses('PostComment', 'Model');
App::uses('PostLike', 'Model');

class SearchesController extends AppController 
{
    public $helpers = ['Paginator'];
    public $components = ['Paginator','Session'];

    public $paginate = [
        'Post' => ['limit' => 10],
        'User' => ['limit' => 10]
    ];

    public function beforeFilter() 
    {
        parent::beforeFilter();
    }
    
    public function index() 
    {
        $type = '';
        $keyword = '';

        if (isset($this->request->data['Search']['type'])) {
            $type = $this->request->data['Search']['type'];
            $this->Session->delete('SearchKeyword');
        } else {
            if($this->Session->check('SearchType')) {
                $type = $this->Session->read('SearchType');
            } else {
                $type = 'user';
            }
        }
        if (isset($this->request->data['Search']['keyword'])) {
            $keyword = $this->request->data['Search']['keyword'];
        } else {
            if($this->Session->check('SearchKeyword')) {
                $keyword = $this->Session->read('SearchKeyword');
            }
        }

        $this->Session->write('SearchType', $type);
        $this->Session->write('SearchKeyword', $keyword);

        $this->set('keyword',$keyword);

        
        if ($type == 'post') {
            $renderForm = 'posts';
            $posts = $this->_getPostList($keyword);
            $this->_getPostComments($posts);
            $this->set('posts', $posts);
        } else {
            $renderForm = 'users';
            $users = $this->_getUsersList($keyword);
            $this->set('users', $users);
        }
        
        $this->render($renderForm);
    }

    private function _getUsersList(string $keyword) : array
    {
        $sessUid = $this->Auth->User('id');

        $this->Paginator->settings = [
            'joins' => [
                [
                    'table' => 'follows',
                    'alias' => 'FollowsJoin',
                    'type' => 'LEFT',
                    'conditions' => [
                        'FollowsJoin.followed_id = User.id',
                        'FollowsJoin.follower_id' => $sessUid
                    ]
                ]
            ],
            'fields' => ['*'],
            'conditions' => [
                'User.activated' => 1,
                'OR' => [
                    ["CONCAT(User.first_name,' ',User.last_name) LIKE" => "%{$keyword}%"],
                    ['User.username LIKE' => "%{$keyword}%"]
                ]
            ],
            'limit' => 10,
            'order' => [
                'User.first_name' => 'asc',
                'User.last_name' => 'asc'
            ]       
        ];
        $User = new User();
        $users = $this->Paginator->paginate($User);
        return $users;
    }


    private function _getPostList(string $keyword) : array
    {
        $sessUid = $this->Auth->User('id');

        $this->Paginator->settings = [
            'joins' => [
                [
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'INNER',
                    'conditions' => [
                        'Post.user_id = User.id'
                    ]
                ],
                [
                    'table' => 'post_likes',
                    'alias' => 'PostLike',
                    'type' => 'LEFT',
                    'conditions' => [
                        'PostLike.post_id = Post.id',
                        'PostLike.user_id' => $sessUid
                    ]
                ],
                [
                    'table' => 'posts',
                    'alias' => 'Reposted',
                    'type' => 'LEFT',
                    'conditions' => [
                        'Post.parent_id = Reposted.id'
                    ]
                ],
                [
                    'table' => 'users',
                    'alias' => 'RepostedUser',
                    'type' => 'LEFT',
                    'conditions' => [
                        'Reposted.user_id = RepostedUser.id'
                    ]
                ]
            ],
            'fields' => ['*'],
            'conditions' => [
                'Post.content LIKE' => "%{$keyword}%",
                'Post.deleted' => 0
            ],
            'limit' => 10,
            'order' => [
                'Post.created' => 'desc'
            ]       
        ];
        $Post = new Post();
        $posts = $this->Paginator->paginate($Post);
        return $posts;
    }

    private function _getPostComments(&$posts) : bool
    {
        $PostComment = new PostComment();
        $PostLike = new PostLike();
        foreach ($posts as $key => $post) {
            $postId = $post['Post']['id'];
            $options = [
                'joins' => [
                    [
                        'table' => 'users',
                        'alias' => 'CommentUser',
                        'type' => 'INNER',
                        'conditions' => [
                            'PostComment.user_id = CommentUser.id'
                        ]
                    ]
                ],
                'fields' => [
                    'PostComment.*',
                    "CONCAT(CommentUser.first_name, ' ',CommentUser.last_name) as fullName"
                ],
                'conditions' => [
                    'PostComment.post_id' => $postId,
                    'PostComment.deleted' => 0
                ]
            ];

            $posts[$key]['comments'] = $PostComment->find('all',$options);

            $options = [
                'conditions' => ['post_id' => $postId]
            ];
            $posts[$key]['likeCount'] = $PostLike->find('count',$options);
        }
        return true;
    }
}