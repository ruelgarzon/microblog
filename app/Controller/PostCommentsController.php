<?php
App::uses('User', 'Model');

class PostCommentsController extends AppController 
{

    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Security->unlockedActions = array('writeComment','deleteComment','editComment');
    }

    public function writeComment() 
    {
        $status = 'error';
        $comment = '';
        $message = '';
        $id = 0;

        if ($this->request->is('post')) {

            $userId = $this->Auth->User('id');
            $this->PostComment->create();
            $this->request->data['PostComment']['user_id'] = $userId;

            if ($this->PostComment->save($this->request->data)) {
                $status = 'success';
                $id = $this->PostComment->id;
                $comment = h($this->request->data['PostComment']['comment']);
            } else {
                $message = parent::renderErrors($this->PostComment->validationErrors);
            }
        }

        $data = [
            'status' => $status,
            'id' => $id,
            'comment' => $comment,
            'message' => $message
        ];
        $this->set(compact('data'));
        $this->layout = false;
        $this->render('json');
    }

    public function deleteComment() 
    {
        $status = 'error';
        $message = '';
        try {
            if ($this->request->is('post')) {
                $id = $this->request->data['PostComment']['id'];

                $this->PostComment->id = $id;
                if (!$this->PostComment->exists()) {
                    throw new Exception('Invalid action. Data not available.');
                }

                $comment = $this->PostComment->findById($id);
                $userId = $this->Auth->User('id');

                if ($comment['PostComment']['user_id'] != $userId) {
                    throw new Exception('Invalid action');
                } 

                $this->PostComment->saveField('deleted', 1);
                $status = 'success';
            }
        } catch (Exception $e) {
           $message = $e->getMessage();
        }
        

        $data = ['status' => $status, 'message' => $message];
        $this->set(compact('data'));
        $this->layout = false;
        $this->render('json');
    }

    public function editComment() 
    {
        $status = 'error';
        $comment = '';
        $message = '';

        if ($this->request->is('post')) {
            $id = $this->request->data['PostComment']['id'];

            $this->PostComment->id = $id;
            if (!$this->PostComment->exists()) {
                throw new NotFoundException(__('Invalid action. Data not available.'));
            }
            if ($this->PostComment->save($this->request->data)) {
               $status = 'success';
               $comment = h($this->request->data['PostComment']['comment']);
            } else {
                $message = parent::renderErrors($this->PostComment->validationErrors);
            }
            
        }

        $data = [
            'status' => $status,
            'comment' => $comment,
            'message' => $message
        ];
        $this->set(compact('data'));
        $this->layout = false;
        $this->render('json');
    }
}