<?php
App::uses('User', 'Model');
App::uses('Follower', 'Model');
App::uses('Following', 'Model');
App::uses('PostComment', 'Model');
App::uses('PostLike', 'Model');
App::uses('Follow', 'Model');

class PostsController extends AppController 
{

    public $helpers = ['Html', 'Form', 'Flash','Paginator'];
    public $components = ['Flash','Paginator'];

    public $paginate = [
            'limit' => 10,
            'order' => ['Post.created' => 'desc']
        ];

    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Security->unlockedActions = array('edit','repost');
    }

    public function index(int $userId = null) 
    {
        $userId = ($userId) ? $userId : $this->Auth->User('id');
        $User = new User();
        $userDetail = $User->findById($userId);

        if (!$userDetail) {
            $this->Flash->error(__('The requested user account is not available.'));
                return $this->redirect(['action' => 'index']);
        }

        $this->set('user', $userDetail);
        $editAccess = ($userId == $this->Auth->User('id')) ? true : false;
        $hideAccess = ($editAccess) ? '' : ' d-none ';
        $this->set('hideAccess', $hideAccess);
        $this->set('editAccess', $editAccess);

        $sessUid = $this->Auth->User('id');
        $followedThisUser = $this->_checkFollowing($userId, $sessUid);
        $this->set('followedThisUser', $followedThisUser);

        $this->set('followers', $this->_getFollower($userId, $sessUid));
        $this->set('followings', $this->_getFollowing($userId, $sessUid));

        $Follower = new Follower();
        $options = ['conditions' => ['Follower.followed_id' => $userId]];
        $this->set('followerCount', $Follower->find('count',$options));

        $Following = new Following();
        $options = ['conditions' => ['Following.follower_id' => $userId]];
        $this->set('followingCount', $Following->find('count',$options));

        if ($editAccess) {
            $userIdArray = $this->_getFollowedIds($userId);
        }
        $userIdArray[] = $userId;

        $this->Paginator->settings = [
            'joins' => [
                [
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'INNER',
                    'conditions' => [
                        'Post.user_id = User.id'
                    ]
                ],
                [
                    'table' => 'post_likes',
                    'alias' => 'PostLike',
                    'type' => 'LEFT',
                    'conditions' => [
                        'PostLike.post_id = Post.id',
                        'PostLike.user_id' => $sessUid
                    ]
                ],
                [
                    'table' => 'posts',
                    'alias' => 'Reposted',
                    'type' => 'LEFT',
                    'conditions' => [
                        'Post.parent_id = Reposted.id'
                    ]
                ],
                [
                    'table' => 'users',
                    'alias' => 'RepostedUser',
                    'type' => 'LEFT',
                    'conditions' => [
                        'Reposted.user_id = RepostedUser.id'
                    ]
                ]
            ],
            'fields' => ['User.*','Post.*','PostLike.id','Reposted.*','RepostedUser.*'],
            'conditions' => [
                'Post.user_id IN' => $userIdArray,
                'Post.deleted' => 0
            ],
            'limit' => 10,
            'order' => ['Post.created' => 'desc']      
        ];
        $posts = $this->Paginator->paginate('Post');
        $this->_getPostComments($posts);
        $this->set('posts', $posts);
    }

    private function _getFollowedIds(int $userId) : array
    {
        $Follow = new Follow();

        $options = [
                'fields' => ['followed_id'],
                'conditions' => [
                    'follower_id' => $userId
                ]
            ];
        return $Follow->find('list',$options);
    }

    private function _checkFollowing(int $userId, int $sessUid) : bool
    {
        $Follow = new Follow();

        $options = [
                'conditions' => [
                    'Follow.followed_id' => $userId,
                    'Follow.follower_id' => $sessUid
                ]
            ];
        $result = $Follow->find('first',$options);
        $return = ($result) ? true : false;
        return $return;
    }

    private function _getPostComments(&$posts) : bool
    {
        $PostComment = new PostComment();
        $PostLike = new PostLike();
        foreach ($posts as $key => $post) {
            $postId = $post['Post']['id'];
            $options = [
                'joins' => [
                    [
                        'table' => 'users',
                        'alias' => 'CommentUser',
                        'type' => 'INNER',
                        'conditions' => [
                            'PostComment.user_id = CommentUser.id'
                        ]
                    ]
                ],
                'fields' => [
                    'PostComment.*',
                    "CONCAT(CommentUser.first_name, ' ',CommentUser.last_name) as fullName"
                ],
                'conditions' => [
                    'PostComment.post_id' => $postId,
                    'PostComment.deleted' => 0
                ]
            ];

            $posts[$key]['comments'] = $PostComment->find('all',$options);

            $options = [
                'conditions' => ['post_id' => $postId]
            ];
            $posts[$key]['likeCount'] = $PostLike->find('count',$options);
        }
        return true;
    }

    private function _getFollower(int $userId, int $sessUid) : array
    {

        $Follower = new Follower();
        $options = [
            'joins' => [
                [
                    'table' => 'follows',
                    'alias' => 'FollowsJoin',
                    'type' => 'LEFT',
                    'conditions' => [
                        'FollowsJoin.followed_id = Follower.follower_id',
                        'FollowsJoin.follower_id' => $sessUid
                    ]
                ]
            ],
            'fields' => ['*'],
            'conditions' => ['Follower.followed_id' => $userId],
            'limit' => 10
        ];

        $result = $Follower->find('all',$options);
        return $result;

    }

    private function _getFollowing(int $userId, int $sessUid) : array
    {
        $Following = new Following();
        $options = [
            'joins' => [
                [
                    'table' => 'follows',
                    'alias' => 'FollowsJoin',
                    'type' => 'LEFT',
                    'conditions' => [
                        'FollowsJoin.followed_id = Following.followed_id',
                        'FollowsJoin.follower_id' => $sessUid
                    ]
                ]
            ],
            'fields' => ['*'],
            'conditions' => ['Following.follower_id' => $userId],
            'limit' => 10
        ];

        $result = $Following->find('all',$options);
        return $result;

    }

    public function add() 
    {
        try {
            if ($this->request->is('post')) {
                $this->Post->create();
                $this->request->data['Post']['user_id'] = $this->Auth->user('id');

                if ($this->request->data['Post']['image_name']['name'] != '' ) {

                    $extn = substr($this->request->data['Post']['image_name']['name'], 
                        strrpos($this->request->data['Post']['image_name']['name'], '.')+1);

                    $image = "post_" . $this->Auth->user('id'). "_" . date('His') .  "." . $extn;

                    $img = WWW_ROOT . 'img/posts/' . $image;
                    
                    move_uploaded_file($this->request->data['Post']['image_name']['tmp_name'], $img);

                    $this->request->data['Post']['image_name'] = $image;
                } else {
                    $this->request->data['Post']['image_name'] = null;
                }
                if ($this->Post->save($this->request->data)) {
                    $this->Flash->success(__('Your post has been saved.'));
                    return $this->redirect($this->referer());
                }
                $errorMsgs = parent::renderErrors($this->Post->validationErrors);
                $this->Flash->error($errorMsgs);
                return $this->redirect($this->referer());

            }
        } catch (Exception $e) {
            $this->Flash->error(
                    __($e->getMessage())
                );
        }
    }

    public function edit() 
    {
        $id = $this->request->data['Post']['id'];
        if (!$id) {
            throw new NotFoundException(__('Invalid action.'));
        }

        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid action.'));
        }

        if ($this->request->data['Post']['remove_image']) {
            $this->_unlinkPostImage($post['Post']['image_name']);
            $this->request->data['Post']['image_name'] = null;
        } else {
            if ($this->request->data['Post']['image_name']['name'] != '' ) {

                $extn = substr($this->request->data['Post']['image_name']['name'], 
                    strrpos($this->request->data['Post']['image_name']['name'], '.')+1);

                $image = "post_" . $this->Auth->user('id'). "_" . date('His') .  "." . $extn;

                $this->_unlinkPostImage($post['Post']['image_name']);
                
                $img = WWW_ROOT . 'img/posts/' . $image;
                move_uploaded_file($this->request->data['Post']['image_name']['tmp_name'], $img);

                $this->request->data['Post']['image_name'] = $image;

            } else {
                unset($this->request->data['Post']['image_name']);
            }
        }

        if ($this->request->is(['post', 'put'])) {
            $this->Post->id = $id;
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been updated.'));
                return $this->redirect($this->referer());
            }
            $errorMsgs = parent::renderErrors($this->Post->validationErrors);
            $this->Flash->error($errorMsgs);
            return $this->redirect($this->referer());
        }

        if (!$this->request->data) {
            $this->request->data = $post;
        }
    }

    private function _unlinkPostImage(string $filename = null)
    {
        // die($filename);
        if ($filename) {
            $oldImg = WWW_ROOT . 'img/posts/' . $filename;
            if (file_exists($oldImg)) {
                unlink($oldImg);
            }
        }
    }

    public function delete($id) 
    {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        $this->Post->id = $id;
        if ( ! $this->Post->exists() ) {
            throw new NotFoundException(
                __('Invalid action. You are not allowed to delete this record.')
            );
        }

        $this->Post->saveField('deleted', 1);

        $this->Flash->success(
                __('Post has been successfully deleted.')
            );
        return $this->redirect($this->referer());
    }
    public function repost() 
    {
        try {
            if ($this->request->is('post')) {
                $this->Post->create();
                $this->request->data['Post']['user_id'] = $this->Auth->user('id');

                if ($this->Post->save($this->request->data)) {
                    $this->Flash->success(__('Your repost has been saved.'));
                    return $this->redirect($this->referer());
                }
                $errorMsgs = parent::renderErrors($this->Post->validationErrors);
                $this->Flash->error($errorMsgs);
                return $this->redirect($this->referer());

            }
        } catch (Exception $e) {
            $this->Flash->error(
                    __($e->getMessage())
                );
        }
    }
}
