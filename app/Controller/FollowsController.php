<?php
App::uses('User', 'Model');

class FollowsController extends AppController 
{

    public function beforeFilter() 
    {
        parent::beforeFilter();
    }
    
    public function follow($id) 
    {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        $userId = $this->Auth->User('id');
        $options = [
            'conditions' => [
                    'Follow.follower_id' => $userId,
                    'Follow.followed_id' => $id
                ]
        ];
        $result = $this->Follow->find('first', $options);

        $User = new User();
        $followedUser = $User->findById($id);
        $followedName = $followedUser['User']['first_name'].' '.$followedUser['User']['last_name'];
        if($result) {
            if ($this->Follow->delete($result['Follow']['id'])) {
                $this->Flash->success(
                    __('You have unfollowed %s', h($followedName))
                );
                return $this->redirect($this->referer());
            }
        } else {
            $this->Follow->create();
            $this->request->data['Follow'] = [
                'follower_id' => $userId,
                'followed_id' => $id
            ];

            if ($this->Follow->save($this->request->data)) {
                $this->Flash->success(__('You have followed %s', h($followedName)));
                return $this->redirect($this->referer());
            }
        }
    }
}