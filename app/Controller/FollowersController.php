<?php
App::uses('User', 'Model');

class FollowersController extends AppController 
{

    public $helpers = ['Html', 'Form', 'Flash','Paginator'];
    public $components = ['Flash','Paginator'];

    public $paginate = [
            'limit' => 10,
            'order' => ['Follower.created' => 'desc']
        ];

    public function beforeFilter() 
    {
        parent::beforeFilter();
    }

    public function index($userId = null) 
    {
        $userId = ($userId) ? $userId : $this->Auth->User('id');
        $User = new User();
        $this->set('user', $User->findById($userId));

        $sessUid = $this->Auth->User('id');

        $this->Paginator->settings = [
            'joins' => [
                [
                    'table' => 'follows',
                    'alias' => 'FollowsJoin',
                    'type' => 'LEFT',
                    'conditions' => [
                        'FollowsJoin.followed_id = Follower.follower_id',
                        'FollowsJoin.follower_id' => $sessUid
                    ]
                ]
            ],
            'fields' => ['*'],
            'conditions' => ['Follower.followed_id' => $userId],
            'limit' => 10,
            'order' => [
                'User.first_name' => 'asc',
                'User.last_name' => 'asc'
            ]       
        ];
        $this->set('followers', $this->Paginator->paginate('Follower'));
    }
}
