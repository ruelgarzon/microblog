<?php
App::uses('User', 'Model');

class PostLikesController extends AppController 
{

    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Security->unlockedActions = array('like');
    }
    
    public function like() 
    {
        $status = 'error';
        $postId = $this->request->data['PostLike']['post_id'];
        $userId = $this->Auth->User('id');
        $options = [
            'conditions' => [
                'PostLike.user_id' => $userId,
                'PostLike.post_id' => $postId
            ]
        ];

        $result = $this->PostLike->find('first', $options);

        if ($result) {
            if ($this->PostLike->delete($result['PostLike']['id'])) {
                $status = 'success';
            }
        } else {
            $this->PostLike->create();
            $this->request->data['PostLike'] = [
                'user_id' => $userId,
                'post_id' => $postId
            ];

            if ($this->PostLike->save($this->request->data)) {
                $status = 'success';
            }
        }

        $data = ['status' => $status];
        $this->set(compact('data'));
        $this->layout = false;
        $this->render('json');
    }
}