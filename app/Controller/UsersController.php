<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController 
{

    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Auth->allow('add','activate');
    }

    public function login() 
    {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    public function logout() 
    {
        return $this->redirect($this->Auth->logout());
    }

    public function add() 
    {
        try {
            if ($this->request->is('post')) {
                $this->User->create();
                 $conditionsUsernames = [
                    'User.username' => $this->request->data['User']['username']
                ];
                 $conditionsEmails = [
                    'User.email' => $this->request->data['User']['email']
                ];

                if ($this->User->hasAny($conditionsUsernames)){
                    $this->Flash->error(
                        __('Username is already in use. Please try another one.')
                    );
                } else if($this->User->hasAny($conditionsEmails)) {
                    $this->Flash->error(
                        __('Email is already in use. Please try another one.')
                    );
                } else {
                    if ($this->User->save($this->request->data)) {
                        $email = $this->request->data['User']['email'];
                        $id = $this->User->id;
                        $firstName = $this->request->data['User']['first_name'];
                        $this->_send_account_activation($email, $id, $firstName);
                        
                        $this->Flash->success(__('Your account has been successfully submitted. Please check your email to activate.'));
                        return $this->redirect(['action' => 'login']);
                    }
                    $errorMsgs = parent::renderErrors($this->User->validationErrors);
                    $this->Flash->error($errorMsgs);
                }

            }
        } catch (Exception $e) {
            $this->Flash->error(
                    __($e->getMessage())
                );
        }
    }

    public function edit() 
    {
        $id = $this->request->data['User']['id'];
        if (!$id) {
            throw new NotFoundException(__('Invalid action.'));
        }

        $user = $this->User->findById($id);
        if (!$user) {
            throw new NotFoundException(__('Invalid action.'));
        }
        if ($this->request->data['User']['profile_picture']['name'] != '' ) {

            $extn = substr($this->request->data['User']['profile_picture']['name'], 
                strrpos($this->request->data['User']['profile_picture']['name'], '.')+1);

            $image = "user_" . $this->Auth->user('id'). "_" . date('His') .  "." . $extn;
            $allowedExtn = 'jpg, jpeg, png, gif';

            if($user['User']['profile_picture']) {
                $oldImg = WWW_ROOT . 'img/profiles/' . $user['User']['profile_picture'];
                if(file_exists($oldImg)) {
                    unlink($oldImg);
                }
            }

            $img = WWW_ROOT . 'img/profiles/' . $image;
            
            move_uploaded_file($this->request->data['User']['profile_picture']['tmp_name'], $img);

            $this->request->data['User']['profile_picture'] = $image;
        } else {
            unset($this->request->data['User']['profile_picture']);
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                
                $this->Session->write('Auth', 
                    $this->User->read(null, 
                        $this->Auth->User('id')));
                
                $this->Flash->success(__('Profile changes have been saved'));
                return $this->redirect(['controller' => 'posts','action' => 'index']);
            }
            $errorMsgs = parent::renderErrors($this->User->validationErrors);
            $this->Flash->error($errorMsgs);
            return $this->redirect(['controller' => 'posts','action' => 'index']);
        } else {
            $this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
    }

    public function changePassword() 
    {
        $id = $this->request->data['User']['id'];
        if (!$id) {
            throw new NotFoundException(__('Invalid action.'));
        }

        $user = $this->User->findById($id);
        if (!$user) {
            throw new NotFoundException(__('Invalid action.'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                
                $this->Flash->success(__('New password has been saved'));
                return $this->redirect(['controller' => 'posts','action' => 'index']);
            }
            $errorMsgs = parent::renderErrors($this->User->validationErrors);
            $this->Flash->error($errorMsgs);
        }
        return $this->redirect(['controller' => 'posts','action' => 'index']);
    }

    public function activate(int $id = null) 
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        } 
        if($this->User->field('activated')) {
            $this->Flash->error(__('Invalid action! Your account is already active.'));
        } else {
            $this->User->saveField('activated', 1);
            $this->Flash->success(__('Congratulations! Your account has been successfully activated.'));
        }
        return $this->redirect(['action' => 'login']);
    }

    private function _send_account_activation(string $email, int $id, string $firstName) : bool
    {
        try {
            $Email = new CakeEmail();
            $Email->viewVars(['id' => $id, 'firstName' => $firstName]);
            $Email->config('default');
            $Email->template('welcome')
                ->emailFormat('html')
                ->to($email)
                ->from(['no-reply@cake-microblog.com' => 'Account Verification'])
                ->subject('MicroBlog account verification.')
                ->send();
            return true;

        } catch (Exception $e) {
            throw $e;
            
        }
    }
}