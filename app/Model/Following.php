<?php
class Following extends AppModel {

    public $useTable = 'follows';
    
    public $belongsTo = [
        'User' => [
            'className' => 'User',
            'foreignKey' => 'followed_id'
        ]
    ];
}