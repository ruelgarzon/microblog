<?php
App::uses('AppModel', 'Model');

class Post extends AppModel 
{
    public $validate = [
        'user_id' => [
            'rule' => 'notBlank'
        ],
        'content' => [
            'maxLength' => [
                'rule' => ['maxLength', 140],
                'message' => 'Post must not be more than 140 characters.'
            ],
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Post content is required'
            ]
        ],
        'image_name' => [
            'extension' =>   [
                'rule' => [
                    'extension',
                    ['gif', 'jpeg', 'png', 'jpg']
                ],
                'message' => 'Please supply a valid image. 
                    Allowed extensions are only gif, jpeg, png and jpg.',
                'allowEmpty' => true 
                ]
        ]
    ];

}