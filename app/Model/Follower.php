<?php
class Follower extends AppModel {

    public $useTable = 'follows';
    
    public $belongsTo = [
        'User' => [
            'className' => 'User',
            'foreignKey' => 'follower_id'
        ]
    ];
}