<?php

class PostComment extends AppModel 
{
    public $validate = [
        'user_id' => [
            'rule' => 'notBlank'
        ],
        'post_id' => [
            'rule' => 'notBlank'
        ],
        'comment' => [
            'maxLength' => [
                'rule' => ['maxLength', 140],
                'message' => 'Comment must not be more than 140 characters.'
            ],
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Comment is required'
            ]
        ]
    ];
}