<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel 
{

    public $validate = [

        'first_name' => [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'First Name is required'
            ],
            'charsOnly' => [
                "rule" => '/^[a-zA-Z]+$/',
                'message' => 'First name do not allow numbers and special characters'
            ]

        ],
        'last_name' => [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'Last Name is required'
            ],
            'charsOnly' => [
                "rule" => ['custom', '/^[a-zA-Z]+$/'],
                'message' => 'Last name do not allow numbers and special characters'
            ]
        ],
        'username' => [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'Username is required'
            ],
            'minChars' => [
                'rule' => ['minLength', 8],
                'message' => 'Username must be atleast 8 characters'
            ]
        ],
        'password' => [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'Password is required'
            ],
            'password_confirm' => [
                'rule' => ['password_confirm'],
                'message' => "Password doesn't match",                         
            ],
            'minChars' => [
                'rule' => ['minLength', 8],
                'message' => 'Password must be atleast 8 characters'
            ]
        ],
        'profile_picture' => [
            'extension' =>   [
                'rule' => [
                    'extension',
                    array('gif', 'jpeg', 'png', 'jpg')
                ],
                'message' => 'Please supply a valid image. 
                    Allowed extensions are only gif, jpeg, png and jpg.',
                'allowEmpty' => true 
                ]
        ],
        'email' => [
            'fieldtype' => [
                'rule' => 'email',
                'message' => 'Email should be valid'
            ],
            'required' => [
                'rule' => 'notBlank',
                'message' => 'Email is required'
            ]
        ]
    ];

    public $hasMany = [
        'Follower' => [
            'className' => 'Follower',
            'foreignKey' => 'follower_id'
        ],
        'Following' => [
            'className' => 'Following',
            'foreignKey' => 'followed_id'
        ]
    ];

    public function beforeSave($options = []) 
    {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }
    public function password_confirm(){ 
        if ($this->data[$this->alias]['password'] !== $this->data[$this->alias]['password_confirm']){
            return false;       
        }
        return true;
    }
}