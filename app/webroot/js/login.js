var Login = function () 
{
    var init = function () 
    {
        if ($('#authenticationAlert #authMessage').length > 0) {
            $('#authenticationAlert').removeClass('d-none');
        }
    }

    return {
        
        init : function () {
            init();
        }

    }
    
}();

Login.init();