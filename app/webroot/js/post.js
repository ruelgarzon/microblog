var Post = function () 
{
    var init = function () 
    {
        Common.uploadInit();
        Common.paginateInit();
        postComment();
        postLike();
    }

    var postComment = function () 
    {
        $('[data-toggle="popover"]').popover();
        $('.postComment').off('click').on('click', function() { 

            var $commentInput = $(this).closest("div.commentBox").find("input[name='commentInput']");
            var $additionalDiv = $(this).closest("div.additionalDiv");
            var $this = $(this);
            let id = $(this).attr("data-id");
            let name = $(this).attr("data-name");
            let comment = $commentInput.val();
            var addedDate = Common.getCurrentDateTime();
            

            if (comment != '') {
                var data = { 
                    'PostComment' : {
                        'post_id' : id,
                        'comment' : comment
                    }
                };
                var webroot = $('#webroot').val();
                $commentInput.val('');
                $.post(webroot + 'post_comments/writeComment', data, function(result)
                {
                    if (result.status == 'success') {
                        updateCommentCount(id, 1);
                        var commentSection = commentTemplate (name, addedDate, result.id, id, result.comment);
                        $additionalDiv.prepend(commentSection);
                        Common.timeInit();
                    } else {
                        Common.notifMsg(result.message, 'error');
                    }
                    
                }, 'json');
                
            } else {
                $commentInput.popover('show');
                $commentInput.focus();
                $commentInput.focusout(function() {
                    $commentInput.popover('hide');
                });
            }
        });
    }
    var commentTemplate = function (name, addedDate, commentId, postId, comment) 
    {
        var commentSection = `<div class="list-group-item list-group-item-action flex-column
            align-items-start line-hieght-10">
            <div class="d-flex w-100 justify-content-between">
                <small class="mb-1 font-weight-bold">
                    ${name}
                </small>
                <small>
                    <time class="timeago" datetime="${addedDate}"></time>
                    <div class="dropup d-inline-block">
                        <a class="btn btn-link text-dark btn-sm" 
                        href="#" 
                        id="dropdownMenuButton" 
                        data-toggle="dropdown" 
                        aria-haspopup="true" aria-expanded="false">
                            <span class="oi oi-ellipses small"></span>
                        </a>
                        <div class="dropdown-menu" 
                        aria-labelledby="dropdownMenuButton">
                            <a href="javascript:;" 
                            class="dropdown-item line-hieght-10"
                            onclick="Post.editComment(this,
                                    ${commentId},
                                    \`${comment}\`
                                )">
                                <span class="oi oi-pencil"></span> 
                                Edit
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="javascript:;" 
                            class="dropdown-item line-hieght-10"
                                onclick="Post.deleteComment(this,
                                    ${commentId},
                                    ${postId}
                                )">
                                <span class="oi oi-trash"></span> 
                                Delete
                            </a>
                        </div>
                    </div>
                </small>
            </div>
            <small class="font-weight-light"
            id="commentElement${commentId}">
                ${comment}
            </small>
        </div>`;

        return commentSection;
    }

    var postLike = function () 
    {
        
        $('.triggerLike').off('click').on('click', function() 
        { 
            let id = $(this).attr("data-id");
            let likeCount = parseInt($(this).attr("data-likeCount"));
            if ($(this).hasClass('post-liked')) {
                $(this).removeClass('post-liked');
                likeCount -= 1;
            } else {
                $(this).addClass('post-liked');
                likeCount += 1;
            }
            let likeLabel = (likeCount > 1) ? ' Likes' : ' Like';
            $(this).attr('data-likeCount',likeCount);
            $(this).closest('.likeContainer').find('.likeCount').html(likeCount + likeLabel);
            var data = { 'PostLike' : {'post_id' : id}};
            var webroot = $('#webroot').val();
            $.post(webroot + 'post_likes/like', data, function(result)
            {

            }, 'json');
           
        });
    }

    var deleteComment = function (that, id, post_id) 
    {
        var data = { 'PostComment' : {'id' : id}};
        var webroot = $('#webroot').val();
        $.post(webroot + 'post_comments/deleteComment', data, function(result)
        {
            if (result.status == 'success') {
                updateCommentCount(post_id, 0);
                $(that).closest("div.list-group-item").remove();
            } else {
                // Common.notifMsg(result.message,'error');
            }
            
        }, 'json');
           
    }
    var updateCommentCount = function (id, add) 
    {
        var commentCntContainer = $('#commentCountId'+id);
        let commentCount = parseInt($('#commentCountId'+id).attr("data-commentCount"));
        
        if (add) {
            commentCount += 1;
        } else {
            commentCount -= 1;
        }
        var countLable = (commentCount > 1) ? ' Comments' : ' Comment';

        commentCntContainer.attr("data-commentCount",commentCount);
        commentCntContainer.html(commentCount+countLable);
    }

    var edit = function (id, content) 
    {
        $('#postModal #PostId').val(id);
        $('#postModal #PostContent').val(content);

        $('.hide-post-attachment').off('change').on('change', function () {
            var $checked = $(this).is(":checked");
            if ($checked === true) {
                $(this).closest('#postModal').find('div.custom-file').addClass('d-none');
            } else {
                $(this).closest('#postModal').find('div.custom-file').removeClass('d-none');
            }
            
        });

        $('#postModal').modal();
    }

    var editComment = function (that, id, content) 
    {
        $('#editComment #CommentId').val(id);
        $('#editComment #CommentContent').val(content);
        $('#editComment').modal();


        $('#editCommentForm').off( "submit.update").on( "submit.update",function(e) {
            e.preventDefault();
            var data = $(this).serialize();
            var webroot = $('#webroot').val();
            $.post(webroot + 'post_comments/editComment', data, function(result)
            {
                if (result.status == 'success') {
                    $(that).attr('onClick',`Post.editComment(this, ${id},\`${result.comment}\`)`);
                    $('#commentElement' + id).html(result.comment);
                    $('#editComment').modal('hide');
                } else {
                    Common.notifMsg(result.message, 'error');
                }

            }, 'json');
        });
    }
    var repost = function (id) 
    {
        $('#repostModal #PostParentId').val(id);
        $('#repostModal').modal();
    }

    return {
        
        init : function () {
            init();
        },
        edit : function (id, content) {
            edit(id, content);
        },
        deleteComment : function (that, id, post_id) {
            deleteComment(that, id, post_id);
        },
        editComment : function (that, id, content) {
            editComment(that, id, content);
        },
        repost : function (id) {
            repost(id);
        }

    }
    
}();

Post.init();