var Search = function () 
{
    var init = function () 
    {
        Common.paginateInit();
    }

    return {
        
        init : function () {
            init();
        }
    }
    
}();

Search.init();