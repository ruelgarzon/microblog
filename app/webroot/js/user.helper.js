var UserHelper = function () 
{
    var init = function () 
    {
        $(document).ready(function(){
            $('.route-user').off('click').on('click', function() { 
                let id = $(this).attr("data-userId");
                var webroot = $('#webroot').val();
                window.location.href = webroot + 'posts/index/' + id;
            });
        });
    }
    
    return {
        
        init : function () {
            init();
        }
    }
    
}();

UserHelper.init();