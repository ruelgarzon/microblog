var Common = function () 
{
    var init = function () 
    {
        $(document).ready(function(){
            constNotif();
            timeInit();
        });
    }

    var constNotif = function () 
    {
        if ($('#alertModal #flashMessage').length > 0) {
            if ($('#alertModal .message.error').length > 0) {
                $('#alertModal div.modal-body').addClass('alert-danger');
            } else {
                $('#alertModal div.modal-body').addClass('alert-success');
            }
            $('#alertModal').modal('show');
        }
        
    }

    var notifMsg = function (message, type = 'success') 
    {
        if (type == 'success') {
            $('#alertModal div.modal-body').addClass('alert-success');
        } else {
            $('#alertModal div.modal-body').addClass('alert-danger');
        }
        $('#alertModal div#notifMsg').html(message);
        $('#alertModal').modal('show');
    }

    var timeInit = function () 
    {
        if ($("time.timeago").length > 0) {
            $("time.timeago").timeago();
        }
    }

    var uploadInit = function () 
    {
        $('.custom-file-input').on('change', function() { 
           let fileName = $(this).val().split('\\').pop(); 
           $(this).next('.custom-file-label').html(fileName); 
        });
    }
    var paginateInit = function ()
    {
        $('ul.pagination li.page-item a').addClass('page-link');
    }

    var getCurrentDateTime = function ()
    {
        var currentdate = new Date(); 
        var dateyear = currentdate.getFullYear();
        var datemonth = (currentdate.getMonth() + 1);
        var dateday = currentdate.getDate();

        datemonth = (datemonth > 10) ? datemonth : '0' + datemonth.toString();
        var time = currentdate.getHours() + ":"  
            + currentdate.getMinutes() + ":" 
            + currentdate.getSeconds();
            
        var addedDate = dateyear + "-" + datemonth + "-" + dateday + " " + time;

        return addedDate;
    }
    
    return {
        
        init : function () {
            init();
        },
        notifMsg : function (message, type = null) {
            notifMsg(message, type);
        },
        uploadInit : function () {
            uploadInit();
        },
        paginateInit : function () {
            paginateInit();
        },
        timeInit : function () {
            timeInit();
        },
        getCurrentDateTime : function () {
            return getCurrentDateTime();
        }

    }
    
}();

Common.init();